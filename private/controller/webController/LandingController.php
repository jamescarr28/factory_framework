<?php

namespace controller\webController;

use abstractClass\AbstractWebController;

/**
 * Created by PhpStorm.
 * User: jamesskywalker
 * Date: 12/10/2019
 * Time: 13:47
 */
class LandingController extends AbstractWebController {


    protected function load() {
        $this->setData('message', 'Hello Fergo');
        $this->setTemplate('landing');
    }

}